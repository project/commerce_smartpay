# Commerce Smartpay

This module allows Drupal Commerce customers to pay using the
Barclaycard Smartpay Hosted Payment page.

https://www.barclaycard.co.uk/business/accepting-payments/payment-gateways

Smartpay HPP is part of the Adyen Payment Platform.
https://www.adyen.com/
This gateway will be compatible with other providers using Hosted Payment Pages.

## Setup

* Use Composer to get the project
```
composer require drupal/commerce_smartpay
```
* Enable the module.

## Configuration

Add a new Payment gateway
``admin/commerce/config/payment-gateways/add``

You will need your Merchant Account, Skin Code and HMAC keys for your Test and Live environments.

### Current Maintainers

* Karen Grey (https://www.drupal.org/u/karengrey)

